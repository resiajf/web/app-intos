import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

import Pagination from 'src/common/components/pagination';
import { State } from 'src/redux';
import { filter, requestAllFiles } from 'src/redux/actions/files';
import { TorrentFile } from 'src/redux/interfaces/files';

interface StateToProps {
    downloadedFiles: TorrentFile[];
    downloadingFiles: TorrentFile[];
    filterDownloaded: string;
    filterDownloading: string;
    hasError: string | null;
    firstLoadDone: boolean;
}

interface DispatchToProps {
    loadFiles: () => void;
    changeFilterDownloaded: (value: string) => void;
    changeFilterDownloading: (value: string) => void;
}

interface Props extends StateToProps, DispatchToProps, RouteComponentProps<{ page: string; }> {
    filterByLocalStorage?: boolean;
}

class DefaultListPage extends Component<Props> {

    private static fId = (file: {id: number, uploaded?: boolean}) => `${file.id}-${file.uploaded ? 'file' : 'torrent'}`;
    private static fUrl = (file: {id: number, uploaded?: boolean}) => `${file.uploaded ? 'file' : 'torrent'}/${file.id}`;

    private interval: number | undefined;

    constructor(props: Props) {
        super(props);

        this.onChangeDownloadingFilter = this.onChangeDownloadingFilter.bind(this);
        this.onChangeDownloadedFilter = this.onChangeDownloadedFilter.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    public componentDidMount() {
        this.interval = setInterval(() => this.props.loadFiles(), 10000) as any;
        if(this.props.firstLoadDone) {
            this.props.loadFiles();
        }
    }

    public componentWillUnmount() {
        clearInterval(this.interval);
    }

    public componentDidUpdate(prevProps: Readonly<Props>) {
        if(prevProps.downloadedFiles.length !== this.props.downloadedFiles.length) {
            this.props.history.push(this.props.match.path.replace(':page', '0'));
        }
    }

    public render() {
        const { downloadedFiles, downloadingFiles, filterDownloaded, filterDownloading, hasError } = this.props;
        const pages = Math.ceil(this.props.downloadedFiles.length / 15);
        const page = Math.max(0, Math.min(pages - 1, (Number(this.props.match.params.page) || 1) - 1));

        if(hasError) {
            clearInterval(this.interval);
            return (
                <div>
                    <h2 className="display-4">No se pudo cargar la lista</h2>
                    <p className="lead">
                        Ha habido un error al cargar la lista y no hemos podido cargarla.
                    </p>
                    <p><i>{ hasError }</i></p>
                </div>
            );
        }

        return (
            <div>
                <div className="row pb-2 pb-sm-0">
                    <div className="col-sm-8"><h2 className="display-4">Archivos descargados</h2></div>
                    <div className="col-sm-4">
                        <input type="text"
                               className="form-control"
                               placeholder="Buscar…"
                               value={filterDownloaded}
                               onChange={ this.onChangeDownloadedFilter } />
                    </div>
                </div>
                <Pagination page={ page } pages={ pages } onChange={ this.changePage } />
                <div className="list-group">
                    {downloadedFiles.slice(page * 15, page * 15 + 15).map(file => {
                        return <Link className="list-group-item list-group-item-action" to={DefaultListPage.fUrl(file)} key={DefaultListPage.fId(file)}>
                            <i className={`fa fa-${file.type}`} />&nbsp;&nbsp;{ file.name }
                        </Link>;
                    })}
                </div>
                <Pagination page={ page } pages={ pages } onChange={ this.changePage } className="mt-2" />
                { filterDownloaded && downloadedFiles.length === 0 ? <div className="text-center">Sin resultados</div> : null }
                { !filterDownloaded && downloadedFiles.length === 0 ? <div className="text-center">No hay nada descargado</div> : null }

                <div style={{ marginTop: '30px' }} />

                <div className="row pb-2 pb-sm-0">
                    <div className="col-sm-8"><h2 className="display-4">Archivos en descarga</h2></div>
                    <div className="col-sm-4">
                        <input type="text"
                               className="form-control"
                               placeholder="Buscar…"
                               value={filterDownloading}
                               onChange={ this.onChangeDownloadingFilter } />
                    </div>
                </div>
                <div className="list-group">
                    {downloadingFiles.map(file => {
                        return <Link className="list-group-item list-group-item-action" to={DefaultListPage.fUrl(file)} key={DefaultListPage.fId(file)}>
                            <i className={`fa fa-${file.type}`} />&nbsp;&nbsp;{ file.name }
                        </Link>;
                    })}
                </div>
                { filterDownloading && downloadingFiles.length === 0 ? <div className="text-center">Sin resultados</div> : null }
                { !filterDownloading && downloadingFiles.length === 0 ? <div className="text-center">No hay nada descargándose</div> : null }
            </div>
        );
    }

    private changePage(page: number) {
        let url;
        if(this.props.match.path.indexOf(':page') !== -1) {
            url = this.props.match.path.replace(':page(\\d+)', `${page + 1}`);
        } else {
            url = this.props.match.path + String(page + 1);
        }
        this.props.history.push(url);
    }

    private onChangeDownloadingFilter(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.changeFilterDownloading(e.target.value);
    }

    private onChangeDownloadedFilter(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.changeFilterDownloaded(e.target.value);
    }

}

const mapStateToProps = (state: State, ownProps: { filterByLocalStorage?: boolean }): StateToProps => ({
    downloadedFiles: (() => {
        const mines = ownProps.filterByLocalStorage ? JSON.parse(window.localStorage.getItem('yourTorrents') || '[]') as number[] : undefined;
        return state.files.files
            .filter(file => file.status === 6 || file.status === 0)
            .filter(file => file.name.toLowerCase().match(state.files.filter.downloaded) !== null)
            .filter(file => !file.uploaded && ownProps.filterByLocalStorage ? mines!.indexOf(Number(file.addedDate)) !== -1 : !ownProps.filterByLocalStorage)
            .sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
    })(),
    downloadingFiles: (() => {
        const mines = ownProps.filterByLocalStorage ? JSON.parse(window.localStorage.getItem('yourTorrents') || '[]') as number[] : undefined;
        return state.files.files
            .filter(file => file.status !== 6 && file.status !== 0)
            .filter(file => file.name.toLowerCase().match(state.files.filter.downloading) !== null)
            .filter(file => !file.uploaded && ownProps.filterByLocalStorage ? mines!.indexOf(Number(file.addedDate)) !== -1 : !ownProps.filterByLocalStorage)
            .sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
    })(),
    filterDownloaded: state.files.filter.downloaded,
    filterDownloading: state.files.filter.downloading,
    firstLoadDone: state.files.firstLoadDone,
    hasError: state.files.hasError,
});

const mapDispatchToProps = (dispatch: any): DispatchToProps => ({
    changeFilterDownloaded: (value) => dispatch(filter(value, 'downloaded')),
    changeFilterDownloading: (value) => dispatch(filter(value, 'downloading')),
    loadFiles: () => dispatch(requestAllFiles()),
});

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(DefaultListPage));
