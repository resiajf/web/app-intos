import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import resumeFtpUpload from 'src/img/resumeFtpUpload.png';

export default class FAQPage extends Component {
    public render() {
        return (
            <div>
                <h2 className="display-4">¿Como funciona esta aplicación?</h2>

                <h3>¿Que ofrece esta web?</h3>
                <hr />
                <p>
                    Esta aplicación web ofrece la posibilidad de compartir entre todos los residentes archivos (como películas, música,
                    etc.) de forma local y fácil. Además también permite poner a descargar torrents que luego se compartirán entre todos
                    o subir archivos vuestros al servicio (<Link to="/upload">más info</Link>). Y por último, facilita la reproducción de
                    peliculas o de canciones sin la necesidad de tenerlos que descargar (<i>véase</i> Reproducción de archivos <i>más
                    abajo</i>).
                </p>
                <br />

                <h3>Reproducción de archivos</h3>
                <hr />
                <p>
                    Como gran parte de películas que se descargan no son compatibles con los navegadores, se ha decidido usar archivos
                    <i>playlist</i> para que se puedan reproducir con un programa adecuado, como <a href="http://www.videolan.org/vlc" target="_blank" rel="noopener noreferrer">
                    VLC</a> o <a href="https://lhc70000.github.io/iina/" target="_blank" rel="noopener noreferrer">iina (macOS)</a>.
                </p>
                <p>
                    Para ver alguna película, haced click en el icono de <i className="fa fa-play-circle" /> y se descargará un archivo
                    cuya extensión es <i>.pls</i>. Ese archivo abridlo con el programa que más os guste. Se recomienda usar VLC por ser
                    muy versátil y estar disponible para cualquier sistema operativo importante. A partir de ese momento, podréis
                    disfrutar de una película sin tener que descargarlo en vuestro ordenador.
                </p>
                <p>
                    Para reproducir una carpeta entera, clickad en el enlace que dice <i><i className="fa fa-play-circle" />
                    Ver/Escuchar carpeta entera</i> y se descargará un archivo parecido al anterior mencionado. Y con el mismo
                    procedimiento, podréis ver todo sin tener que descargar nada más que ese pequeño archivo. El orden de reproducción
                    será el mismo orden que aparecen los archivos con <i className="fa fa-play-circle" /> en la web.
                </p>
                <p>
                    <b>Para navegantes de Safari:</b> Este navegador (el que viene con un Mac por defecto) los archivos <i>.pls</i> no
                    los descarga por defecto y te permite reproducirlos en el navegador. Si veis que un archivo no lo reproduce,
                    forzad la descarga con Click Derecho (o Ctrl+Click) y apretad <i>Descargar archivo enlazado</i>.
                </p>
                <br/>

                <h3>¿Como funciona esto de los torrents?</h3>
                <hr/>
                <p>
                    Tenemos un cliente de torrent (<i>Transmission</i>) abierto a todas horas y descargando. Desde esta web se puede
                    añadir torrents a descargar en el servidor para que no te tengas que preocupar de descargarlo tu, ni de dejar el
                    ordenador encendido o te tengas que ir. El único limite es la velocidad de descarga durante el dia: de 12h de la
                    mañana a 12h de la noche, el torrent no descarga a toda velocidad para no estorbar a los estudiantes, durante la
                    noche, no hay más limite que la propia velocidad de internet.
                </p>
                <p>
                    Para añadir un torrent a descargar, necesitas el archivo <i>.torrent</i>, un link un torrent o un enlace magnet.
                    Vas a la página de <Link to="/add-torrent">Añadir para descargar</Link> para ponerlo a descargar, y en la pestaña de
                    <Link to="/yours">Tus torrents</Link> podrás ver el progreso de los torrents que has puesto a descargar.
                    Fácil, ¿no?
                </p>
                <br/>

                <h3>¿Por que no es posible ver esta web desde fuera de la red de la residencia?</h3>
                <hr/>
                <p>
                    Por motivos legales, no deberiamos exponer esta web más allá de la propia red de la Residencia.
                </p>
                <br/>

                <h3>Si algo falla, ¿que hago?</h3>
                <hr/>
                <p>
                    Contacta con los becarios de informática
                </p>
                <br/>

                <h3>Tengo una pregunta que no aparece aquí o quiero sugerir algo para mejorar la web, ¿que hago?</h3>
                <hr/>
                <p>
                    Contacta con los becarios de informática, mejor si buscáis al creador de la web :)
                </p>
                <br/>

                <h3>¿Que hago cuando se ha cortado una subida por FTP?</h3>
                <hr/>
                <p>
                    Si usas FileZilla, el propio programa te permite retomar la subida desde donde lo dejaste o se te cortó. Simplemente
                    vuelve a pasar el archivo que subias de nuevo y te pedirá que hacer, ya que dicho archivo ya existe, y seleccionas
                    la opción de renaudar.
                    <img src={ resumeFtpUpload } alt="Dialogo de acción sobre la existencia del mismo archivo en FileZilla" style={{width: '100%'}} />
                </p>
            </div>
        );
    }
}