import { FilesActions, FilesState } from 'src/redux/interfaces/files';
import {
    FILTER, LOAD_MEDIA_INFO, REQUEST_ALL_FILES, REQUEST_INFO_FILE, RESPONSE_ALL_FILES, RESPONSE_ERROR,
    RESPONSE_INFO_FILE
} from '../actions/files';

const files = (state: FilesState = {
    detailedFile: null,
    files: [],
    filter: {
        downloaded: '',
        downloading: '',
    },
    firstLoadDone: false,
    hasError: null,
    mediaInfo: {
        torrent: {},
        upload: {}
    }
}, action: FilesActions): FilesState => {
    switch(action.type) {
        case REQUEST_ALL_FILES:
            return {
                ...state,
                hasError: null
            };

        case RESPONSE_ALL_FILES:
            return {
                ...state,
                files: action.files ? action.files : [],
                firstLoadDone: true
            };

        case REQUEST_INFO_FILE:
            const file = state.files.filter(file => file.id === action.id && (!action.uploaded || file.uploaded))[0];
            const sameFile = file && state.detailedFile && file.id === state.detailedFile.id && file.uploaded === state.detailedFile.uploaded;
            return {
                ...state,
                detailedFile: sameFile ? state.detailedFile : file as any, //TODO
                hasError: null
            };

        case RESPONSE_INFO_FILE:
            return {
                ...state,
                detailedFile: action.info
            };

        case FILTER:
            if(action.filterType === 'downloading') {
                return {
                    ...state,
                    filter: {
                        ...state.filter,
                        downloading: action.value
                    }
                };
            } else if(action.filterType === 'downloaded') {
                return {
                    ...state,
                    filter: {
                        ...state.filter,
                        downloaded: action.value
                    }
                };
            } else {
                return state;
            }

        case RESPONSE_ERROR:
            const hasError = action.error;
            return { ...state, hasError };

        case LOAD_MEDIA_INFO:
            return {
                ...state,
                mediaInfo: {
                    ...state.mediaInfo,
                    [action.rtype]: {
                        ...state.mediaInfo[action.rtype],
                        [action.id]: {
                            ...(state.mediaInfo[action.rtype][action.id] || {}),
                            [action.file]: action.info
                        }
                    }
                }
            };

        default:
            return state;
    }
};

export default files;
