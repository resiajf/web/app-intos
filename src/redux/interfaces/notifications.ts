import { Action } from 'redux';

import { NOTIFICATION, NOTIFICATION_CLOSED } from 'src/redux/actions/notifications';

export interface NotificationsState {
    list: Map<number, { kind: string; msg: string; }>;
}

interface NotificationAction extends Action<typeof NOTIFICATION> {
    kind: string;
    msg: string;
    toastId: number;
}

interface NotificationClosedAction extends Action<typeof NOTIFICATION_CLOSED> {
    toastId: number;
}

export type NotificationsActions = NotificationAction | NotificationClosedAction;