import React from 'react';
import { toast, ToastContent, ToastOptions } from 'react-toastify';
import { Dispatch } from 'redux';

export const NOTIFICATION = 'NOTIFICATION';
export const NOTIFICATION_CLOSED = 'NOTIFICATION:CLOSED';

const ToastContent = ({ toastId, children }: { toastId: number; children: string; }) => <span>{ children }</span>;

const showNotification = (msg: string, kind: string, toastFunc: (c: ToastContent, d?: ToastOptions) => number, dispatch: Dispatch) => {
    const toastId = toastFunc(msg, {
        onClose: (({ toastId }: { toastId: number }) => {
            dispatch({
                toastId,
                type: NOTIFICATION_CLOSED,
            });
        }) as () => void,
    });
    dispatch({
        kind,
        msg,
        toastId,
        type: NOTIFICATION,
    });
    setTimeout(() => {
        toast.update(toastId, {
            render: <ToastContent toastId={ toastId }>{ msg }</ToastContent>,
        });
    }, 1);
};

export const info = (msg: string) => (dispatch: Dispatch) => {
    showNotification(msg, toast.TYPE.INFO, (a: any, b: any) => toast.info(a, b), dispatch);
};

export const warning = (msg: string) => (dispatch: Dispatch) => {
    showNotification(msg, toast.TYPE.WARNING, toast.warn.bind(toast), dispatch);
};

export const error = (msg: string) => (dispatch: Dispatch) => {
    showNotification(msg, toast.TYPE.ERROR, toast.error.bind(toast), dispatch);
};

export const success = (msg: string) => (dispatch: Dispatch) => {
    showNotification(msg, toast.TYPE.SUCCESS, toast.success.bind(toast), dispatch);
};

export const dismiss = (id: number) => {
    toast.dismiss(id);
    return {
        toastId: id,
        type: NOTIFICATION_CLOSED,
    };
};
