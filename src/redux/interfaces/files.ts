import { Action } from 'redux';

import {
    FILTER, LOAD_MEDIA_INFO,
    REQUEST_ALL_FILES,
    REQUEST_INFO_FILE,
    RESPONSE_ALL_FILES,
    RESPONSE_ERROR,
    RESPONSE_INFO_FILE
} from 'src/redux/actions/files';


export interface TorrentFile {
    addedDate: number;
    eta: -1 | number;
    id: number;
    name: string;
    totalSize: number;
    percentDone?: number;
    recheckProgress?: number;
    rateDownload?: number;
    rateUpload?: number;
    status: 0 | 1 | 2 | 3 | 4 | 5 | 6;
    uploaded: undefined |true;
    type: string;
}

interface DetailedOneFile {
    bytesCompleted?: number;
    id: number;
    length: number;
    name: string;
    type: string;
}

export interface DetailedTorrentFile extends TorrentFile {
    files: DetailedOneFile[];
}

export interface MediaInfoFile {
    duration: number;
    videos: Array<{
        id: number;
        num: number;
        codec: string;
        width: number;
        height: number;
        bitRate: number;
        fps: number;
        name: string;
    }>;
    audios: Array<{
        id: number;
        num: number;
        codec: string;
        bitRate: number;
        sampleRate: number;
        bitDepth: number;
        channels: number;
        language: string | null;
        name: string | null;
    }>;
    subtitles: Array<{
        id: number;
        num: number;
        language: string | null;
        title: string | null;
    }>;
}

export interface FilesState {
    files: TorrentFile[];
    firstLoadDone: boolean;
    detailedFile: DetailedTorrentFile | null;
    filter: {
        downloading: string;
        downloaded: string;
    };
    hasError: any | null;
    mediaInfo: {
        torrent: { [index: number]: { [index: number]: MediaInfoFile } },
        upload: { [index: number]: { [index: number]: MediaInfoFile } }
    };
}

interface RequestAllFilesAction extends Action<typeof REQUEST_ALL_FILES> {

}

interface ResponseAllFilesAction extends Action<typeof RESPONSE_ALL_FILES> {
    files: TorrentFile[];
}

interface RequestInfoFileAction extends Action<typeof REQUEST_INFO_FILE> {
    id: string | number;
    uploaded: boolean;
}

interface ResponseInfoFileAction extends Action<typeof RESPONSE_INFO_FILE> {
    id: string | number;
    info: DetailedTorrentFile;
}

interface ResponseErrorAction extends Action<typeof RESPONSE_ERROR> {
    error: any;
}

interface FilterAction extends Action<typeof FILTER> {
    filterType: 'downloaded' | 'downloading';
    value: string;

}

interface LoadMediaInfoAction extends Action<typeof LOAD_MEDIA_INFO> {
    info: MediaInfoFile;
    rtype: 'upload' | 'torrent';
    id: number;
    file: number;
}

export type FilesActions = RequestAllFilesAction | ResponseAllFilesAction | RequestInfoFileAction |
    ResponseInfoFileAction | ResponseErrorAction | FilterAction | LoadMediaInfoAction;
