import React from 'react';

import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'intos';

const FAQPage = asyncComponent(() => import('src/components/FAQPage'));
const UploadPage = asyncComponent(() => import('src/components/UploadPage'));
const AddTorrentPage = asyncComponent(() => import('src/containers/AddTorrentPage'));
const DefaultListPage = asyncComponent(() => import('src/containers/DefaultListPage'));
const FilePage = asyncComponent(() => import('src/containers/FilePage'));
const YourListPage = (props: any) => <DefaultListPage filterByLocalStorage={ true } {...props} />;

export const routes: Array<Route<any, any>> = [
    route('/', 'all', DefaultListPage, 'home', { exact: true }),
    route('/:page(\\d+)', 'all', DefaultListPage, 'homeWithPage', { hide: true }),
    route('/:type(torrent|file)/:fileId(\\d+)', 'all', FilePage, 'item', { hide: true }),
    route('/upload', 'all', UploadPage, 'upload', { exact: true }),
    route('/faq', 'all', FAQPage, 'faq', { exact: true }),
    route('/add-download', 'all', AddTorrentPage, 'add-download', { exact: true }),
    route('/yours', 'all', YourListPage, 'yours', { exact: true }),
    route('/yours/:page(\\d+)', 'all', YourListPage, 'yoursWithPage', { hide: true }),
];