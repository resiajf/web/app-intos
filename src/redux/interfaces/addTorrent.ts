import { Action } from 'redux';
import {
    ADDED_TORRENT,
    ADDING_TORRENT,
    CHANGE_TORRENT_FILE,
    CHANGE_TORRENT_URL,
    HIDE_MESSAGE,
    RESET,
    SHOW_MESSAGE
} from 'src/redux/actions/addTorrent';

export interface AddTorrentState {
    url: string;
    alert: {
        class: string | null,
        message: string | null,
    };
    file: File | null;
    loading: boolean;
}



interface ChangeTorrentUrlAction extends Action<typeof CHANGE_TORRENT_URL> {
    url: string;
}

interface ShowMessageAction extends Action<typeof SHOW_MESSAGE> {
    class: string;
    message: string;
}

type HideMessageAction = Action<typeof HIDE_MESSAGE>;

type ResetAction = Action<typeof RESET>;

interface ChangeTorrentFileAction extends Action<typeof CHANGE_TORRENT_FILE> {
    files: FileList;
}

interface ChangeTorrentUrlAction extends Action<typeof CHANGE_TORRENT_URL> {
    url: string;
}

type AddingTorrentAction = Action<typeof ADDING_TORRENT>;

interface AddedTorrentAction extends Action<typeof ADDED_TORRENT> {
    ok: boolean;
    result: { data: { addedDate: number | null; }; };
}

export type AddTorrentActions = ChangeTorrentUrlAction | ShowMessageAction | HideMessageAction | ResetAction |
    ChangeTorrentFileAction | ChangeTorrentUrlAction | AddingTorrentAction | AddedTorrentAction;