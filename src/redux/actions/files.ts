import { Dispatch } from 'redux';

import { BASE_URL } from 'src/config';
import { error } from './notifications';

export const REQUEST_ALL_FILES = 'REQUEST_ALL_FILES';
export const RESPONSE_ALL_FILES = 'RESPONSE_ALL_FILES';
export const REQUEST_INFO_FILE = 'REQUEST_INFO_FILE';
export const RESPONSE_INFO_FILE = 'RESPONSE_INFO_FILE';
export const RESPONSE_ERROR = 'RESPONSE_ERROR';
export const FILTER = 'FILTER';
export const LOAD_MEDIA_INFO = 'LOAD_MEDIA_INFO';

const errorCodesToString = (err: { code: number; error?: string; id?: number; }) => {
    switch(err.code) {
        case 1: return `Error al conectar con el servidor torrent (${err.error})`;
        case 2: return `No existe el torrent con id ${err.id}`;
        case 3: return `No existe el archivo con id ${err.id}`;
        case 4: return 'No se ha añadido un enlace para descargar';
        case 5: return 'No se ha seleccionado un archivo .torrent para descargar';
        default: return `Ha ocurrido algo que no conocemos: ${err.error}`;
    }
};

const showError = (err: any, dispatch: Dispatch) => {
    if(typeof(err) !== 'string') {
        if(err.error !== undefined) {
            err = errorCodesToString(err);
        } else {
            err = JSON.stringify(err);
        }
    }
    // @ts-ignore
    dispatch(error(err));
    dispatch({ type: RESPONSE_ERROR, error: err });
};

export const requestAllFiles = () => (dispatch: Dispatch) => {
    dispatch({ type: REQUEST_ALL_FILES });
    fetch(`${BASE_URL}/torrents`)
        .then(
            response => {
                if(response.ok) {
                    return response.json();
                } else {
                    const contentType = response.headers.get('Content-Type');
                    if(!contentType || contentType.indexOf('json') === -1) {
                        return response.text().then(text => showError(text, dispatch));
                    } else {
                        return response.json().then(error => showError(error, dispatch));
                    }
                }
            },
            error => showError(error, dispatch)
        ).then(response => {
            if(response !== undefined) {
                dispatch({
                    files: response.results || response,
                    type: RESPONSE_ALL_FILES,
                });
            }
        }
    );
};

export const requestInfoFile = (id: string | number, uploaded: boolean) => (dispatch: Dispatch) => {
    if(typeof id === 'string') { id = Number(id); }
    dispatch({ type: REQUEST_INFO_FILE, id, uploaded });
    fetch(`${BASE_URL}/${ uploaded ? 'info_upload' : 'info_torrent' }/${id}`)
        .then(
            response => {
                if(response.ok) {
                    return response.json();
                } else {
                    const contentType = response.headers.get('Content-Type');
                    if(!contentType || contentType.indexOf('json') === -1) {
                        return response.text().then(text => showError(text, dispatch));
                    } else {
                        return response.json().then(error => showError(error, dispatch));
                    }
                }
            },
            error => showError(error, dispatch)
        ).then(response => {
            dispatch({
                id,
                info: response,
                type: RESPONSE_INFO_FILE,
            });
        }
    );
};

export const filter = (value: string, type: 'downloaded' | 'downloading') => ({
    filterType: type,
    type: FILTER,
    value: value.toLowerCase(),
});

export const loadMediaInfo = (type: 'upload' | 'torrent', id: number, file: number) => (dispatch: Dispatch) => {
    fetch(`${BASE_URL}/mediainfo/${type}/${id}/${file}`)
        .then(response => {
            if(!response.ok) {
                dispatch(error('No se pudo cargar la información') as any);
                return undefined;
            } else {
                return response.json();
            }
        })
        .then(info => dispatch({ type: LOAD_MEDIA_INFO, info, rtype: type, id, file }));
};
