import { Dispatch } from 'redux';

import { BASE_URL } from 'src/config';

export const CHANGE_TORRENT_URL = 'CHANGE_TORRENT_URL';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const HIDE_MESSAGE = 'HIDE_MESSAGE';
export const RESET = 'RESET';
export const CHANGE_TORRENT_FILE = 'CHANGE_TORRENT_FILE';
export const ADDING_TORRENT = 'ADDING_TORRENT';
export const ADDED_TORRENT = 'ADDED_TORRENT';

export const changeTorrentUrl = (url: string) => ({
    type: CHANGE_TORRENT_URL,
    url
});

export const showMessage = (message: string, type: string, duration: number) => (dispatch: Dispatch) => {
    dispatch({
        class: type,
        message,
        type: SHOW_MESSAGE,
    });

    if(!isNaN(duration) && isFinite(duration)) {
        setTimeout(() => {
            dispatch({ type: HIDE_MESSAGE });
        }, duration);
    }
};

export const reset = () => ({
    type: RESET
});

export const changeTorrentFile = (files: FileList) => ({
    files,
    type: CHANGE_TORRENT_FILE,
});

export const addTorrent = (obj: File | string) => (dispatch: Dispatch | any) => {
    let promise;
    dispatch({ type: ADDING_TORRENT });
    if(typeof(obj) === 'string') {
        dispatch(showMessage('Añadiendo URL de torrent para descargar...', 'info', Infinity));
        promise = fetch(`${BASE_URL}/add_torrent`, {
            body: JSON.stringify({ url: obj }),
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            method: 'POST',
        });
    } else {
        dispatch(showMessage('Añadiendo archivo torrent para descargar...', 'info', Infinity));
        const formData = new FormData();
        formData.append('file', obj);
        promise = fetch(`${BASE_URL}/add_torrent`, {
            body: formData,
            method: 'POST',
        });
    }
    promise.then(
        result => result.json(),
        error => {
            dispatch({ type: ADDED_TORRENT, ok: false, error });
            dispatch(showMessage('Ha habido un error al añadir el torrent', 'danger', 3000));
        }
    ).then(result => {
        dispatch({
            ok: true,
            result,
            type: ADDED_TORRENT,
        });
        dispatch(showMessage('Se ha añadido correctamente', 'success', 3000));
    });
};
