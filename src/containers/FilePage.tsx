import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';

import Helmet from 'react-helmet';
import { BASE_URL } from 'src/config';
import { State } from 'src/redux';
import { loadMediaInfo, requestInfoFile } from 'src/redux/actions/files';
import { DetailedTorrentFile, MediaInfoFile } from 'src/redux/interfaces/files';

const $ = require('jquery');

interface StateToProps {
    file: DetailedTorrentFile | null;
    infos: { [index: number]: MediaInfoFile };
    hasError: any | null;
}

interface DispatchToProps {
    requestInfoFile: () => void;
    loadMediaInfo: (file: number) => void;
}

type OwnProps = RouteComponentProps<{ type: 'file' | 'torrent'; fileId: string; }>;

type Props = StateToProps & DispatchToProps & OwnProps;

class FilePage extends Component<Props> {

    private static showMediaInfo(i: number, info: MediaInfoFile) {
        const sr = (audio: typeof info.audios[0]) => (audio.sampleRate / 1000).toFixed(1) + 'KHz';
        const codec = (codec: string) => {
            switch(codec) {
                case 'AVC':  return 'H264';
                case 'HEVC': return 'H265';
                default:     return codec;
            }
        };
        const bitRate = (a: { bitRate: number }) => {
            const br = a.bitRate;
            if(br < 1000) {
                return `${br}bps`;
            } else if(br < 1000000) {
                return `${(br / 1000).toFixed(2).replace(/\.?0+$/, '')}Kbps`;
            } else if(br < 10000000000) {
                return `${(br / 1000000).toFixed(2).replace(/\.?0+$/, '')}Mbps`;
            } else {
                return `${(br / 10000000000).toFixed(2).replace(/\.?0+$/, '')}Gbps`;
            }
        };
        const duration = (duration: number) => {
            const int = Math.trunc(duration);
            const ms = (duration - int).toFixed(3).substr(2);
            const s = int % 60;
            const m = Math.trunc(duration / 60) % 60;
            const h = Math.trunc(duration / 3600);
            if(h < 1) {
                // @ts-ignore padStart
                return `${m.toString().padStart(2, '0')}:${s.toString().padStart(2, '0')}.${ms}`;
            } else {
                // @ts-ignore padStart
                return `${h}:${m.toString().padStart(2, '0')}:${s.toString().padStart(2, '0')}.${ms}`;
            }
        };
        const sel = $('#media-info-' + i);
        if(info === undefined) {
            sel.popover('dispose').popover({
                content: '<p>No se pudo cargar la información</p>',
                html: true,
                trigger: 'focus'
            }).popover('show').on('hidden.bs.popover', () => sel.popover('dispose'));
            return;
        }
        sel.popover('dispose').popover({
            content: `<b>Duración:</b> ${duration(info.duration)}<br/>` +
                info.videos.map(video => `<b>Video: </b>${video.width}x${video.height} ${codec(video.codec)} <small class="text-muted">${bitRate(video)}</small>`).join('<br/>') +
                (info.videos.length > 0 ? '<br/>' : '') +
                info.audios.map(audio => `<b>Audio ${audio.num}:</b> ${audio.language || ''} ${audio.codec} <small class="text-muted">${bitRate(audio)} ${sr(audio)}-${audio.bitDepth}</small>`).join('<br/>') +
                (info.audios.length > 0 ? '<br/>' : '') +
                info.subtitles.map(sb => `<b>Subtítulo ${sb.num}:</b> ${sb.language} <small class="text-muted">${sb.title || ''}</small>`).join('<br/>'),
            html: true,
            trigger: 'focus'
        }).popover('show').on('hidden.bs.popover', () => sel.popover('dispose'));
    }

    private interval: number | undefined;
    private waitingForMediaInfo: undefined | number;

    constructor(props: Props) {
        super(props);

        this.mediaInfoButtonClicked = this.mediaInfoButtonClicked.bind(this);
    }

    public componentDidMount() {
        this.props.requestInfoFile();
        this.interval = setInterval(() => {
            this.props.requestInfoFile();
        }, 1000) as any;
    }

    public componentDidUpdate(prevProps: Readonly<Props>) {
        if(this.waitingForMediaInfo !== undefined) {
            const i = this.waitingForMediaInfo;
            if(!prevProps.infos || prevProps.infos[i] !== this.props.infos[i]) {
                this.waitingForMediaInfo = undefined;
                const info = this.props.infos[i];
                FilePage.showMediaInfo(i, info);
            }
        }
    }

    public componentWillUnmount() {
        clearInterval(this.interval);
        $('.media-info').popover('dispose');
    }

    public render() {
        const { file, hasError } = this.props;
        const prettySize = (size: number): string => {
            let str = '';

            if(isNaN(size)) {
                str = 'Nada';
            } else if(size <= 1024) {
                str = size + 'B';
            } else if(size < 1024 * 1024) {
                str = (size / 1024).toFixed(2) + 'KB';
            } else if(size < 1024 * 1024 * 1024) {
                str = (size / 1024 / 1024).toFixed(2) + 'MB';
            } else {
                str = (size / 1024 / 1024 / 1024).toFixed(2) + 'GB';
            }

            return str;
        };
        const isMedia = (file: string): boolean => {
            const ext = file.substr(file.lastIndexOf('.') + 1);
            const is = ext.match(/^(mp3)?(mp4)?(m4a)?(ogg)?(flac)?(alac)?(wav)?(avi)?(mkv)?$/i);
            return !!is;
        };

        if(!file && !hasError) {
            return (
                <div>
                    <h1 className="display-4">Cargando...</h1>
                </div>
            );
        } else if(hasError) {
            clearInterval(this.interval);
            return (
                <div>
                    <h1 className="display-4">No se pudo cargar</h1>
                    <p className="lead">
                        Ha habido algún error en el servidor y no hemos podido cargar esta información.
                    </p>
                    <p><i>{ hasError }</i></p>
                </div>
            );
        } else if(file!['result'] && !hasError) {
            clearInterval(this.interval);
            return (
                <div>
                    <h1 className="display-4">Este archivo no existe</h1>
                    <p className="lead">
                        Revisa que la URL sea correcta. Es posible que se haya borrado o movido el archivo mientras lo
                        estabas mirando, aunque es raro que sea eso.
                    </p>
                </div>
            );
        } else if(file) {
            if(file.uploaded) {
                clearInterval(this.interval);
            }

            const addedDateObject = new Date(file.addedDate * 1000);
            const etaDate = file.eta !== -1 ? new Date(+ new Date() + file.eta * 1000) : null;
            return (
                <>
                    <Helmet>
                        <title>{ file.name }</title>
                    </Helmet>
                    <h1 className="display-4">{ file.name }</h1>
                    { file.status === 4 &&
                    <div className="progress">
                        <div className="progress-bar"
                             role="progressbar"
                             aria-valuenow={ file.percentDone! * 100 }
                             aria-valuemin={ 0 }
                             aria-valuemax={ 100 }
                             style={{ width: (file.percentDone! * 100) + '%' }}>
                            { Math.trunc(file.percentDone! * 100) }%
                        </div>
                    </div>}
                    { file.status === 2 &&
                    <div className="progress">
                        <div className="progress-bar bg-warning progress-bar-striped progress-bar-animated"
                             role="progressbar"
                             aria-valuenow={ file.recheckProgress! * 100 }
                             aria-valuemin={ 0 }
                             aria-valuemax={ 100 }
                             style={{ width: (file.recheckProgress! * 100) + '%' }}>
                            { Math.trunc(file.recheckProgress! * 100) }%
                        </div>
                    </div> }

                    <div className="row">
                        <div className="col-4 col-sm-3 col-lg-2 text-right"><b>Añadido el:</b></div>
                        <div className="col-8 col-sm-9 col-lg-10 d-flex align-items-center">
                            { moment(addedDateObject).format('LLL') }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4 col-sm-3 col-lg-2 text-right"><b>Tamaño total:</b></div>
                        <div className="col-8 col-sm-9 col-lg-10 d-flex align-items-center">
                            { prettySize(file.totalSize) }
                        </div>
                    </div>
                    { file.status === 4 &&
                    <>
                    <div className="row">
                        <div className="col-4 col-sm-3 col-lg-2 text-right"><b>Tiempo restante aproximado:</b></div>
                        <div className="col-8 col-sm-9 col-lg-10 d-flex align-items-center">
                            { moment(etaDate!).fromNow(true) }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4 col-sm-3 col-lg-2 text-right"><b>Velocidad de descarga:</b></div>
                        <div className="col-8 col-sm-9 col-lg-10 d-flex align-items-center">
                            { prettySize(file.rateDownload!) }/s
                        </div>
                    </div>
                    </>
                    }
                    { file.uploaded !== true &&
                    <div className="row">
                        <div className="col-4 col-sm-3 col-lg-2 text-right"><b>Velocidad de subida:</b></div>
                        <div className="col-8 col-sm-9 col-lg-10 d-flex align-items-center">
                            { prettySize(file.rateUpload!) }/s
                        </div>
                    </div>
                    }
                    { isMedia(file.name) &&
                    <div className="d-flex justify-content-center" style={{ marginTop: 15, marginBottom: 15 }}>
                        <a href={`${BASE_URL}/pls/${file.uploaded ? 'upload' : 'torrent'}/${file.id}`} target="_blank">
                            <i className="fa fa-play-circle" />&nbsp;
                            Ver/Escuchar carpeta entera
                        </a>
                    </div>
                    }
                    <div className="text-center">
                        <b>Archivos</b>
                    </div>

                    <div className="list-group">
                        { file.files && file.files
                            .sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))
                            .map((subFile) =>
                                <div className="list-group-item list-group-item-action" key={ subFile.id }>
                                    <div className="row">
                                        <div className="col truncate">
                                            <i className={`fa fa-${subFile.type}`} />&nbsp;
                                            <a href={`${BASE_URL}/${file.uploaded ? 'download_file2' : 'download_file'}/${file.id}/${subFile.id}`}>
                                                { subFile.name.replace(file.name + '/', '') }
                                            </a>
                                            { isMedia(subFile.name) && (file.status === 6 || file.status === 0) &&
                                            <a href={`${BASE_URL}/pls/${file.uploaded ? 'upload' : 'torrent'}/${file.id}/${subFile.id}`}
                                               target="_blank"
                                               className="pull-right">
                                                <i className="fa fa-play-circle" />
                                            </a>
                                            }
                                            { isMedia(subFile.name) && (file.status === 6 || file.status === 0) &&
                                            <a href="#" id={`media-info-${subFile.id}`} className="pull-right media-info"
                                               style={{ marginRight: 5 }}
                                               onClick={ this.mediaInfoButtonClicked(subFile.id) }>
                                                <i className="fa fa-info-circle" />
                                            </a>
                                            }
                                        </div>
                                        <div className="col-auto">
                                            { prettySize(subFile.length) }
                                        </div>
                                        { file.status === 4 &&
                                        <div className="col-12">
                                            <div className="progress">
                                                <div className="progress-bar bg-success" role="progressbar"
                                                     style={{width: (subFile.bytesCompleted! / subFile.length * 100) + '%'}}>
                                                    { (subFile.bytesCompleted! / subFile.length * 100).toFixed(2) }%
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>
                            ) }
                    </div>
                </>
            );
        } else {
            throw new Error('xD'); //TODO
        }
    }

    private mediaInfoButtonClicked(i: number) {
        return (e: React.MouseEvent<HTMLAnchorElement>) => {
            e.preventDefault();
            if(this.props.infos && this.props.infos[i]) {
                const info = this.props.infos[i];
                FilePage.showMediaInfo(i, info);
            } else {
                this.props.loadMediaInfo(i);
                this.waitingForMediaInfo = i;
                $('#media-info-' + i).popover('dispose').popover({
                    content: 'Cargando…'
                }).popover('show');
            }
        };
    }

}

const mapStateToProps = (state: State, ownProps: OwnProps): StateToProps => ({
    file: state.files.detailedFile,
    hasError: state.files.hasError,
    infos: state.files.mediaInfo[ownProps.match.params.type === 'file' ? 'upload' : 'torrent'][ownProps.match.params.fileId]
});

const mapDispatchToProps = (dispatch: any, ownProps: OwnProps): DispatchToProps => ({
    loadMediaInfo: (file) => dispatch(loadMediaInfo(ownProps.match.params.type === 'file' ? 'upload' : 'torrent', Number(ownProps.match.params.fileId), file)),
    requestInfoFile: () => dispatch(requestInfoFile(ownProps.match.params.fileId, ownProps.match.params.type === 'file')),
});

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(FilePage));