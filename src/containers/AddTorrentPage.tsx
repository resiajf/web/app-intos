import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { State } from 'src/redux';
import { addTorrent, changeTorrentFile, changeTorrentUrl, reset } from 'src/redux/actions/addTorrent';

interface StateToProps {
    torrentUrl: string;
    alertMsg: string | null;
    alertClass: string | null;
    loading: boolean;
    torrentFile: File | null;
}

interface DispatchToProps {
    addTorrent: (torrent: string | File) => void;
    changeTorrentFile: (value: FileList) => void;
    changeTorrentUrl: (value: string) => void;
    resetAddTorrentState: () => void;
}

type Props = StateToProps & DispatchToProps;

class AddTorrentPage extends Component<Props> {

    constructor(props: any) {
        super(props);

        this.onTorrentFileChange = this.onTorrentFileChange.bind(this);
        this.onTorrentUrlChange = this.onTorrentUrlChange.bind(this);
        this.onAddTorrentFile = this.onAddTorrentFile.bind(this);
        this.onAddTorrentUrl = this.onAddTorrentUrl.bind(this);
    }

    public componentDidMount() {
        this.props.resetAddTorrentState();
    }

    public render() {
        const { torrentUrl, alertMsg, alertClass, loading, torrentFile } = this.props;
        const isValid = !torrentUrl || torrentUrl.match(/(https?:\/\/[a-z.]+\/[a-z0-9/-]+)|(magnet:\?xt=.+)/i) !== null;

        return (
            <div>
                <div className="page-header mb-4">
                    <h1 className="display-4">Añadir archivos para descargar</h1>
                    <span className="lead">
                        Pon a descargar un archivo al servidor usando un enlace a un <i>.torrent</i> o enlace <i>magnet</i> o subiendo
                        el <i>.torrent</i> directamente. Se pondrá a descargar inmediatamente y podrás ver su progreso en
                        <Link to="/yours"> Tus Torrents</Link>. Para subir archivos ya descargados (o no por torrent), tenemos otra
                        alternativa <Link to="/upload">aquí</Link>.
                    </span>
                </div>

                {alertMsg &&
                <div style={{ padding: '5px 40px' }}>
                    <div className={`alert alert-${alertClass}`}> { /* danger success info */ }
                        {alertMsg}
                    </div>
                </div>
                }

                <h3>Enlace a .torrent</h3>
                <div className="input-group">
                    <input className={'form-control' + (!isValid ? ' is-invalid' : '') }
                           type="text"
                           id="torrentUrl"
                           value={torrentUrl}
                           placeholder="Url..."
                           onChange={ this.onTorrentUrlChange } />
                    <div className="input-group-append">
                        <button type="button"
                                className="btn btn-outline-primary"
                                onClick={ this.onAddTorrentUrl }
                                disabled={loading || !isValid || !torrentUrl}>
                            Añadir a descargar
                        </button>
                    </div>
                </div>

                <h3 className="mt-3">Sube el .torrent</h3>
                {
                    /* http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/  boton bonito */
                    /* Drag & Drop http://www.sitepoint.com/html5-file-drag-and-drop/ */
                }
                <div className="input-group" role="group" aria-label="Subir archivo">
                    <div className="input-group-prepend">
                        <span className="btn btn-outline-primary btn-file">
                            <input type="file" id="torrentFile" onChange={ this.onTorrentFileChange } accept="application/x-bittorrent" />
                            Buscar archivo
                        </span>
                    </div>
                    <input type="text" value={ (torrentFile && torrentFile.name) || 'Selecciona un archivo...'} className="form-control" disabled={ true } />
                    <div className="input-group-append">
                        <button type="button"
                                className="btn btn-outline-primary"
                                onClick={ this.onAddTorrentFile }
                                disabled={loading || !torrentFile}>Subir y añadir a descargar</button>
                    </div>
                </div>

            </div>
        );
    }

    private onTorrentUrlChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.changeTorrentUrl(e.target.value);
    }

    private onTorrentFileChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.changeTorrentFile(e.target.files!);
    }

    private onAddTorrentUrl(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.addTorrent(this.props.torrentUrl);
    }

    private onAddTorrentFile(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.addTorrent(this.props.torrentFile!);
    }

}

const mapStateToProps = (state: State): StateToProps => ({
    alertClass: state.addTorrent.alert.class,
    alertMsg: state.addTorrent.alert.message,
    loading: state.addTorrent.loading,
    torrentFile: state.addTorrent.file,
    torrentUrl: state.addTorrent.url,
});

const mapDispatchToProps = (dispatch: any): DispatchToProps => ({
    addTorrent: obj => dispatch(addTorrent(obj)),
    changeTorrentFile: file => dispatch(changeTorrentFile(file)),
    changeTorrentUrl: value => dispatch(changeTorrentUrl(value)),
    resetAddTorrentState: () => dispatch(reset()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddTorrentPage);
