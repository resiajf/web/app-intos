import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';
import { AddTorrentState } from 'src/redux/interfaces/addTorrent';
import { FilesState } from 'src/redux/interfaces/files';
import { NotificationsState } from 'src/redux/interfaces/notifications';
import addTorrent from 'src/redux/reducers/addTorrent';
import files from 'src/redux/reducers/files';
import notifications from 'src/redux/reducers/notifications';

export const reducers = combineReducers<State>({
    ...commonReducers,
    addTorrent,
    files,
    notifications,
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    addTorrent: AddTorrentState;
    files: FilesState;
    notifications: NotificationsState;
}
