import {
    ADDED_TORRENT,
    ADDING_TORRENT, CHANGE_TORRENT_FILE, CHANGE_TORRENT_URL, HIDE_MESSAGE, RESET,
    SHOW_MESSAGE
} from 'src/redux/actions/addTorrent';
import { AddTorrentActions, AddTorrentState } from 'src/redux/interfaces/addTorrent';

const initialState: AddTorrentState = {
    alert: {
        class: null,
        message: null,
    },
    file: null,
    loading: false,
    url: '',
};

const addTorrent = (state = initialState, action: AddTorrentActions) => {
    switch(action.type) {
        case RESET:
            return initialState;

        case SHOW_MESSAGE:
            return {
                ...state,
                alert: {
                    class: action.class,
                    message: action.message,
                }
            };

        case CHANGE_TORRENT_URL:
            return {
                ...state,
                url: action.url
            };

        case HIDE_MESSAGE:
            return {
                ...state,
                alert: initialState.alert
            };

        case CHANGE_TORRENT_FILE:
            return {
                ...state,
                file: action.files[0]
            };

        case ADDING_TORRENT:
            return {
                ...state,
                loading: true
            };

        case ADDED_TORRENT:
            const added = JSON.parse(window.localStorage.getItem('yourTorrents') || '[]');
            added.push((action.result.data || { addedDate: +new Date() }).addedDate);
            window.localStorage.setItem('yourTorrents', JSON.stringify(added));
            return {
                ...state,
                loading: false
            };

        default:
            return state;
    }
};

export default addTorrent;
