import { NotificationsActions, NotificationsState } from 'src/redux/interfaces/notifications';
import { NOTIFICATION, NOTIFICATION_CLOSED } from '../actions/notifications';

const initialState: NotificationsState = {
    list: new Map()
};

const notifications = (state = initialState, action: NotificationsActions): NotificationsState => {
    switch(action.type) {
        case NOTIFICATION:
            return {
                ...state,
                list: new Map(state.list).set(action.toastId, {
                    kind: action.kind,
                    msg: action.msg
                })
            };

        case NOTIFICATION_CLOSED:
            const map = new Map(state.list);
            map.delete(action.toastId);
            return {
                ...state,
                list: map
            };

        default:
            return state;
    }
};

export default notifications;
