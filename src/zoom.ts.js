/*
 zoom.ts v7.5.0
 https://www.michael-bull.com/projects/zoom.ts

 Copyright (c) 2017 Michael Bull (https://www.michael-bull.com)
 @license ISC
*/
/* eslint-disable */
/* tslint:disable */
(function() {
    function I(a, b) {
        a = a.getAttribute("data-" + b);
        return null === a || (a = Number(a), isNaN(a)) ? Infinity : a
    }
    function J(a) {
        void 0 === a && (a = window);
        void 0 === a.pageYOffset ? (a = a.document, a = (a.compatMode === K ? a.documentElement : a.body).scrollTop) : a = a.pageYOffset;
        return a
    }
    function Y(a) {
        "function" !== typeof a.preventDefault && (a.preventDefault = function() {
            a.returnValue = !1
        });
        "function" !== typeof a.stopPropagation && (a.stopPropagation = function() {
            a.cancelBubble = !0
        });
        if ("mouseover" === a.type) {
            var b = a;
            void 0 ===
            b.relatedTarget && void 0 !== b.fromElement && (b.relatedTarget = b.fromElement)
        } else
            "mouseout" === a.type && (b = a, void 0 === b.relatedTarget && void 0 !== b.toElement && (b.relatedTarget = b.toElement));
        return a
    }
    function u(a, b) {
        "function" === typeof a ? a(b) : a.handleEvent(b)
    }
    function t(a, b, c, d) {
        function e(a) {
            if (void 0 === a)
                if (void 0 !== window.event)
                    a = window.event;
                else
                    throw Error("No current event to handle.");
            u(c, Y(a))
        }
        void 0 === d && (d = !1);
        var g = a.addEventListener,
            f = a.attachEvent;
        if ("function" === typeof g)
            return g.call(a, b, e,
                d), e;
        if ("function" === typeof f && f.call(a, "on" + b, e))
            return e
    }
    function p(a, b, c) {
        var d = a.removeEventListener,
            e = a.detachEvent;
        "function" === typeof d ? d.call(a, b, c) : "function" === typeof e && e.call(a, b, c)
    }
    function Z(a) {
        return function(b) {
            27 === b.keyCode && (b.preventDefault(), b.stopPropagation(), u(a, b))
        }
    }
    function aa(a, b, c, d) {
        return function(e) {
            Math.abs(a - c()) >= b && u(d, e)
        }
    }
    function L(a, b, c) {
        var d = t(window, "scroll", aa(J(), a.H, J, c)),
            e = t(document, "keyup", Z(c)),
            g = t(b, "click", c);
        return function() {
            p(window, "scroll", d);
            p(document, "keyup", e);
            p(b, "click", g)
        }
    }
    function v(a, b, c) {
        var d;
        void 0 === d && (d = !1);
        var e = t(a, b, function(d) {
            void 0 !== e && p(a, b, e);
            u(c, d)
        }, d);
        return e
    }
    function ba(a) {
        var b = document;
        "complete" === b.readyState ? a() : v(b, "DOMContentLoaded", function() {
            return a()
        })
    }
    function M() {
        var a = document,
            a = a.compatMode === K ? a.documentElement : a.body;
        return [a.clientWidth, a.clientHeight]
    }
    function N(a) {
        var b = document.createElement("div");
        b.className = a;
        return b
    }
    function ca(a) {
        var b = "" + a.charAt(0).toUpperCase() + a.substr(1),
            c = da.map(function(a) {
                return "" +
                    a + b
            });
        return [a].concat(c)
    }
    function y(a, b) {
        var c = 0;
        for (b = ca(b); c < b.length; c++) {
            var d = b[c];
            if (d in a)
                return d
        }
    }
    function q(a, b) {
        return {
            position: a,
            size: b
        }
    }
    function O(a) {
        a = a.getBoundingClientRect();
        return q(z(a), [a.width, a.height])
    }
    function A(a, b, c, d, e) {
        a.left = b;
        a.top = c;
        a.width = d;
        a.maxWidth = d;
        a.height = e
    }
    function w(a, b) {
        var c = b.position;
        b = b.size;
        A(a, c[0] + "px", c[1] + "px", b[0] + "px", b[1] + "px")
    }
    function x(a, b) {
        var c = M();
        var d = b.size;
        a = P([Math.min(c[0], a[0]), Math.min(c[1], a[1])], b.size);
        d = [d[0] * a, d[1] * a];
        return {
            position: Q(c,
                q(b.position, d)),
            size: d
        }
    }
    function z(a) {
        return [a.left, a.top]
    }
    function B(a, b) {
        return [a[0] / b, a[1] / b]
    }
    function C(a, b) {
        return [a[0] - b[0], a[1] - b[1]]
    }
    function P(a, b) {
        a = [a[0] / b[0], a[1] / b[1]];
        return Math.min(a[0], a[1])
    }
    function Q(a, b) {
        return C(B(C(a, b.size), 2), b.position)
    }
    function D(a, b, c, d) {
        var e = M();
        c = P([Math.min(e[0], c[0]), Math.min(e[1], c[1])], d.size);
        var g = d.size;
        g = [g[0] * c, g[1] * c];
        var f = d.position;
        d = B(C(d.size, g), 2);
        e = B(Q(e, q([f[0] + d[0], f[1] + d[1]], g)), c);
        b.style[a.l] = a.C ? "scale3d(" + c + ", " + c + ", 1) " + ("translate3d(" +
            e[0] + "px, " + e[1] + "px, 0)") : "scale(" + c + ") " + ("translate(" + e[0] + "px, " + e[1] + "px)")
    }
    function R(a, b, c) {
        a.style[b] = "initial";
        c();
        a.offsetHeight;
        a.style[b] = ""
    }
    function S(a, b, c) {
        return 0 < a.length && c.indexOf(a) === b
    }
    function ea(a) {
        return function(b, c, d) {
            return b !== a && S(b, c, d)
        }
    }
    function n(a, b) {
        return -1 !== a.className.indexOf(b)
    }
    function k(a, b) {
        a.className = a.className.split(" ").concat(b).filter(S).join(" ")
    }
    function m(a, b) {
        a.className = a.className.split(" ").filter(ea(b)).join(" ")
    }
    function fa(a, b) {
        var c = document.createElement("img");
        c.className = a.A;
        c.src = b;
        v(c, "load", function() {
            return k(c, a.i)
        });
        return c
    }
    function ga(a, b) {
        return function() {
            if (void 0 !== b.clone) {
                var c = n(b.b, a.g);
                !n(b.clone, a.j) && c && E(a, b.c, b.clone)
            }
        }
    }
    function T(a, b, c) {
        void 0 !== b.clone && void 0 !== c && (n(b.clone, a.i) || p(b.clone, "load", c))
    }
    function E(a, b, c) {
        k(c, a.j);
        k(b, a.v)
    }
    function ha(a, b, c) {
        m(b, a.v);
        m(c, a.j)
    }
    function F(a, b) {
        a = a.getAttribute("data-src");
        return null === a ? b.src : a
    }
    function U(a, b) {
        document.body.appendChild(b);
        b.offsetHeight;
        k(b, a.w)
    }
    function G(a, b) {
        void 0 !==
        b.clone && ha(a, b.c, b.clone);
        document.body.removeChild(b.f);
        m(b.c, a.o);
        m(b.b, a.s);
        b.b.style.height = "";
        setTimeout(function() {
            return V(a)
        }, 1)
    }
    function ia(a, b, c, d) {
        var e = O(b.c),
            g = t(window, "resize", function() {
                var a = z(b.b.getBoundingClientRect());
                e = q(a, e.size);
                w(b.a.style, x(c, e))
            });
        var f = L(a, b.a, function() {
            f();
            p(window, "resize", g);
            T(a, b, d);
            m(b.b, a.g);
            A(b.a.style, "", "", "", "");
            G(a, b)
        });
        U(a, b.f);
        k(b.b, a.g);
        b.b.style.height = b.c.height + "px";
        k(b.c, a.o);
        w(b.a.style, x(c, e))
    }
    function ja(a, b, c, d, e) {
        function g() {
            void 0 !==
            b.clone && n(b.clone, a.i) && !n(b.clone, a.j) && (void 0 !== d && p(b.clone, e.m, d), E(a, b.c, b.clone));
            m(b.b, a.h);
            k(b.b, a.g);
            R(b.a, e.transitionProperty, function() {
                b.a.style[e.l] = "";
                w(b.a.style, x(c, f))
            })
        }
        var f = O(b.c),
            h = t(window, "resize", function() {
                var d = z(b.b.getBoundingClientRect());
                f = q(d, f.size);
                d = b.b;
                n(d, a.h) || n(d, a.s) ? D(e, b.a, c, f) : w(b.a.style, x(c, f))
            });
        var ka = L(a, b.a, function() {
            ka();
            p(window, "resize", h);
            T(a, b, d);
            m(b.f, a.w);
            k(b.b, a.s);
            var g = v(b.a, e.m, function() {
                G(a, b)
            });
            n(b.b, a.h) ? (void 0 !== r && p(b.a, e.m, r),
                b.a.style[e.l] = "", m(b.b, a.h)) : (R(b.a, e.transitionProperty, function() {
                D(e, b.a, c, f)
            }), b.a.style[e.l] = "", A(b.a.style, "", "", "", ""), m(b.b, a.g));
            void 0 === g && G(a, b)
        });
        U(a, b.f);
        k(b.b, a.h);
        b.b.style.height = b.c.height + "px";
        k(b.c, a.o);
        var r = v(b.a, e.m, function() {
            return g()
        });
        void 0 === r ? g() : D(e, b.a, c, f)
    }
    function V(a) {
        var b = t(document.body, "click", function(c) {
            var d = c.target;
            if (d instanceof HTMLImageElement && null !== d.parentElement && null !== d.parentElement && null !== d.parentElement.parentElement && n(d, a.F)) {
                c.preventDefault();
                c.stopPropagation();
                var e = b,
                    d = c.target,
                    g = d.parentElement,
                    f = n(g, a.u),
                    g = f ? g.parentElement : g;
                if (c.metaKey || c.ctrlKey)
                    window.open(F(g, d), "_blank");
                else {
                    void 0 !== e && p(document.body, "click", e);
                    c = N(a.G);
                    if (f) {
                        var h = d.parentElement;
                        var f = h.parentElement,
                            k;
                        F(f, d) !== d.src && (k = h.children.item(1));
                        h = {
                            f: c,
                            b: f,
                            a: h,
                            c: d,
                            clone: k
                        }
                    } else
                        k = N(a.u), f = d.parentElement, e = F(f, d), e !== d.src && (h = fa(a, e)), h = {
                            f: c,
                            b: f,
                            a: k,
                            c: d,
                            clone: h
                        }, h.b.replaceChild(h.a, d), h.a.appendChild(d), void 0 !== h.clone && h.a.appendChild(h.clone);
                    k = void 0;
                    void 0 !== h.clone && (n(h.clone, a.i) ? E(a, d, h.clone) : k = t(h.clone, "load", ga(a, h)));
                    var d = h.b,
                        d = [I(d, "width"), I(d, "height")],
                        f = document.body.style;
                    c = y(f, "transform");
                    f = y(f, "transition");
                    e = !1;
                    void 0 !== c && (e = !0);
                    g = !1;
                    if (void 0 !== f) {
                        var r = la[f];
                        g = void 0 !== r
                    }
                    var l = !1;
                    if (void 0 !== c)
                        if (l = document.body.style, void 0 !== y(l, "perspective"))
                            if ("WebkitPerspective" in l) {
                                l = document.createElement("div");
                                l.id = H;
                                l.style.position = "absolute";
                                var m = document.createElement("style");
                                m.textContent = ma;
                                var q = document.body;
                                q.appendChild(m);
                                q.appendChild(l);
                                var u = l.offsetWidth,
                                    v = l.offsetHeight;
                                q.removeChild(m);
                                q.removeChild(l);
                                l = u === W && v === X
                            } else
                                l = !0;
                        else
                            l = !1;
                    r = {
                        l: c,
                        transitionProperty: f,
                        m: r,
                        B: e,
                        C: l,
                        D: g
                    };
                    r.B && r.D ? ja(a, h, d, k, r) : ia(a, h, d, k)
                }
            }
        })
    }
    var K = "CSS1Compat",
        da = ["Webkit", "Moz", "ms", "O"],
        H = "test3d",
        W = 4,
        X = 8,
        ma = "" + ("#" + H + "{margin:0;padding:0;border:0;width:0;height:0}") + "@media (transform-3d),(-webkit-transform-3d){" + ("#" + H + "{width:" + (W + "px") + ";height:" + (X + "px") + "}") + "}",
        la = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            msTransition: "MSTransitionEnd",
            transition: "transitionend"
        },
        na = {
            H: 50,
            A: "zoom__clone",
            j: "zoom__clone--visible",
            i: "zoom__clone--loaded",
            u: "zoom__container",
            F: "zoom__element",
            v: "zoom__element--hidden",
            o: "zoom__element--active",
            G: "zoom__overlay",
            w: "zoom__overlay--visible",
            I: "zoom",
            h: "zoom--expanding",
            g: "zoom--expanded",
            s: "zoom--collapsing"
        };
    (function(a) {
        void 0 === a && (a = na);
        ba(function() {
            return V(a)
        })
    })()
})();

