import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import ftp1 from 'src/img/ftp1.png';
import ftp2 from 'src/img/ftp2.png';
import ftp3 from 'src/img/ftp3.png';

export default class UploadPage extends Component {
    public render() {
        return (
            <div>
                <h2 className="display-4">Sube archivos a la web</h2>
                <p className="lead">
                    Si deseas compartir cualquier archivo a esta web, sea una serie, película u otra cosa, y no se puede
                    hacer por descargar por torrent, tenemos una solución para ello.
                    Sube cualquier cosa, la única limitación es el espacio. Para ello debes usar un cliente FTP (<i>programa para poder
                    acceder a carpetas remotas</i>). En esta sección te contamos que necesitas para conectarte y te recomendamos
                    un programa para aquellos que no sepáis como se hace.
                </p>

                <br />
                <h3>Información sobre la conexión FTP</h3>
                <hr />
                <p>
                    Para conectarse al FTP y subir cualquier cosa, lo necesario son los datos de conexión y un cliente FTP:
                </p>
                <div className="container mb-2">
                    <div className="row">
                        <div className="col-3 text-right"><b>Servidor:</b></div>
                        <div className="col-9"><code>10.10.0.1</code> o <code>10.10.20.1</code></div>
                    </div>
                    <div className="row">
                        <div className="col-3 text-right"><b>Usuario:</b></div>
                        <div className="col-9"><code>subidas</code></div>
                    </div>
                    <div className="row">
                        <div className="col-3 text-right"><b>Contraseña:</b></div>
                        <div className="col-9"><code>subidas</code></div>
                    </div>
                </div>
                <p>
                    <b>OJO CUIDADO:</b> Esto es solo para conexiónes dentro de la red de la Residencia, fuera
                    de ella no funcionará.<br />
                    <b>ADVERTENCIA:</b> Los archivos subidos no se pueden borrar almenos que contactes con los becarios de informática.
                    Esto está hecho para evitar que otras personas borren sin querer (<i>o queriendo</i>) algún archivo.<br />
                    <b>RECOMENDACIÓN:</b> Para las subidas se recomienda usar una conexión por cable, en
                    lugar de WiFi.
                </p>

                <br />
                <h3>Normas de organización del FTP</h3>
                <hr />
                <p>
                    No exigimos mucho para subir archivos al FTP, solo los siguientes:
                </p>
                <ol>
                    <li>
                        Si se suben varios archivos relacionados entre sí, metedlos todos dentro de una carpeta con nombre significativo.
                        (Ej.: One Piece, que tiene toda la serie (<i>anime</i>) completa; o Star Wars, que tiene toda las películas)
                    </li>
                    <li>
                        Si se sube un solo archivo, no es obligatorio meterlo dentro de una carpeta como lo anterior, se puede dejar
                        en la carpeta principal (o raíz)
                    </li>
                </ol>

                <br />
                <h3>[Windows, Linux, OS X] Conectarse al FTP usando FileZilla (<i>Recomendado</i>)</h3>
                <hr />
                <p>
                    Primero de todo, si no teneis FileZilla instalado, teneis que descargarlo de la web:&nbsp;
                    <a href="https://filezilla-project.org/download.php?show_all=1" target="_blank" rel="noopener noreferrer">Descargar FileZilla</a>.
                    Para Windows se recomienda descargar el programa que acaba en <code>-setup.exe</code>, si quereis simplemente
                    tenerlo ahi sin instalarlo, descargad el de abajo, que es un zip, si no, os lo instalais.
                </p>
                <p>
                    Para conectarse al FTP simplemente teneis que crear una conexión con la información proporcionada arriba, guardarlo
                    y entrar en el servidor. Veamos como se hace con fotos:
                </p>
                <div className="row">
                    <div className="col-sm-6 col-md-4">
                        <figure className="figure zoom">
                            <img src={ ftp1 } alt="FileZilla + Paso 1" className="figure-img img-fluid rounded zoom__element" />
                            <figcaption className="figure-caption">
                                Esta es la ventana de FileZilla. Para conectarse, vamos a guardar una conexión en FileZilla para no
                                tener que estar todo el tiempo poniendo los datos. Apretamos el botón señalado por la flecha roja
                                (CTRL+S)
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-sm-6 col-md-4">
                        <figure className="figure zoom">
                            <img src={ ftp2 } alt="Creando una nueva conexión" className="figure-img img-fluid rounded zoom__element" />
                            <figcaption className="figure-caption">
                                Para crear esta conexión, primero apretamos el botón de Nueva Conexión y le ponemos un nombre, o no.
                                Luego, en <i>Servidor</i> se pone <code>10.10.10.1</code> como en la imágen. Más abajo tendréis el modo de
                                acceso. Por defecto está en <i>Anónimo</i>, lo cambiais a <i>Normal</i> y poneis el usuario y contraseña
                                <code>subidas</code>, como se muestra en la imágen.
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-sm-6 col-md-4">
                        <figure className="figure zoom">
                            <img src={ ftp3 } alt="Conectándose" className="figure-img img-fluid rounded zoom__element" />
                            <figcaption className="figure-caption">
                                En esta ventana, apretad el botón de <b>Conectar</b> y se os conectará al servidor. Las próximas veces,
                                teneis que ir a esta ventana que abrimos y darle a <b>Conectar</b>. Si todo ha ido bien, veremos el
                                mensaje de <i>Conectado</i> en la parte de arriba tal y como se muestra en la imágen, y luego en el
                                panel de la derecha tendremos los archivos actualmente subidos allí.
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <p>
                    En FileZilla, para subir uno o varios archivos, arrastradlos al panel de la derecha directamente y veréis que abajo
                    en la <i>Archivos en Cola</i> se pondrán a subir todos los archivos y veréis su progreso de subida.<br/>
                    <b>RECORDAD</b> seguir las normas de organización, que no son muy complicadas.<br/>
                    <b>Se me ha cortado la subida y no puedo borrarlo, ¿que hago?</b>&nbsp;
                    <Link to="/faq">Ve a FAQ y baja, alli esta tu respuesta</Link>
                </p>
            </div>
        );
    }
}